%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%%              ------------------------------------------
%%               TWO-THIRDS: yet another Tufte-like class
%%              ------------------------------------------
%% 
%% COMMENTS ----------------------------------------------------------
%%  A significant amount of tweaks comes from:
%%  https://tex.stackexchange.com/a/275626/153854
%%  and some definition based on the classic-thesis class
%%
%% OPTIONS -----------------------------------------------------------
%%  <one,two>side,                        % the margin is put on the outside
%%  compilation=<lualatex,pdflatex,auto>, % specify engine or let me figure it out (longer compilation)
%%  emphColor=<8F1010>,                   % choose the emphasis color (html only)
%%  nofonts,                              % do not redefine fonts
%%
%%
%% PROVIDED COMMANDS -------------------------------------------------
%%  - \loadgeometry{<default,myTufte>}       % with or without large margin
%%  - \color{<emphColor,lightgray,darkgray>} % the main color scheme
%%  - \chaptertoc                            % mini-toc for chapter
%%  - \begin{chapterpage}[<chapterTitle>]    % Automatic chapter page style with chaptertoc
%%     <Introductory text>
%%    \end{chapterpage}
%%  - \begin{fullwidth}                      % Full width box
%%     <text and what not>
%%    \end{fullwidth}
%%  - \marginstomp{<text and what not>}      % Full width box
%%  - \sidecite[<prenote]{<citeKey>}         % Citation in margin
%%  - \allcapsTufte{<text>}                  % wide caps
%%  - \smallcapsTufte{<text>}                % fancy small caps
%%  - \allcaps{<text>}                       % normal caps
%%  - \smallcaps{<text>}                     % normal small caps
%%
%%
%% TODO --------------------------------------------------------------
%%  [ ] Provide choice for margin side (use reversemarginpar with package geometry)
%%  [ ] Make sure that the fullwidth environement is robust with respect to the (one,two)side option
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BASE ===============================================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{two-thirds}[2018/10/16 What I liked in tufte-latex]
% -----
\RequirePackage{silence}   % 
\RequirePackage{amsthm}    % to avoid further collisions
\RequirePackage{xcolor}    % 
\RequirePackage{tikz}    % 
\usetikzlibrary{calc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OPTIONS ============================================================

\RequirePackage{xkeyval}   % the key=value options is needed
\RequirePackage{ifthen}    % options handling

% Compilation --------------------------------------------------------
% Automatically detecting engine feels slow and costly. 
% Here we let the user explicitly choose
\newboolean{compilauto}% 
\setboolean{compilauto}{true}
% Below, idea from xkeyval manual
\newboolean{compillua}% 
\define@choicekey{tufte-ish.cls}{compilation}[\val\nr]{lualatex,pdflatex,auto}{%
 \ifcase\nr\relax                % \AtBeginDocument{compilation: LUA}%
  \setboolean{compillua}{true}
 \or                             % \AtBeginDocument{compilation: PDF}%
  \setboolean{compillua}{true}
 \or                             % \AtBeginDocument{compilation: AUTO}%
  \setboolean{compilauto}{true}
 \fi
}

% Fonts --------------------------------------------------------------
% Let the user do yheir thing
\newboolean{dofonts}
\setboolean{dofonts}{true}
\DeclareOptionX{nofonts}{
 \setboolean{dofonts}{false}
}

% Margins ------------------------------------------------------------
% Reading on a fixed side margin might be preferred
\newboolean{oneSidedPlease}
\setboolean{oneSidedPlease}{false}
\DeclareOptionX{oneside}{%    > geometry does all the job
 \setboolean{oneSidedPlease}{true}
 \PassOptionsToClass{oneside}{book}
} 

% Colors -------------------------------------------------------------
% Let th user choose the main color must be HTML format though
\definecolor{theEmphColor}{HTML}{8F1010} 
\DeclareOptionX{emphColor}[8F1010]{   
 \definecolor{theEmphColor}{HTML}{#1} 
}

% The rest of them options go to the book class ----------------------
\DeclareOptionX*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptionsX%\relax
\LoadClass[]{book}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COLORS =============================================================

\colorlet{emphColor}{theEmphColor}% see options
\colorlet{lightgray}{black!15}
\colorlet{darkgray}{black!75}
%\definecolor{dualColor}{HTML}{3B8686}% sea blue-green
%\definecolor{         }{HTML}{0B486B}% deep blue

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LAYOUT =============================================================

\RequirePackage{geometry}
%\normalbaselineskip=14pt
\geometry{
 a4paper,%showframe,
 left=24.8mm,top=27.4mm,bottom=24.8mm,%textheight=49\baselineskip,
 headheight=\baselineskip,headsep=2\baselineskip,
 textwidth=164.6mm,%107mm+8.2mm+49.4mm,
 marginparsep=2mm,marginparwidth=2mm,
}
\savegeometry{default}
\geometry{%showframe,
 textwidth=107mm,marginparsep=8.2mm,marginparwidth=49.4mm,
}
\savegeometry{myTufte}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FONTS AND ENCODING _________________________________________________

% --------------------------------------------------------------------
\ifthenelse{\boolean{dofonts}}%
{% run only if the user is okay with it

 % Common requirements -----------------------------------------------
 \RequirePackage{anyfontsize}% fixes size errors by scaling
 \RequirePackage[libertine]{newtxmath}
 \RequirePackage[
  sfdefault=iwona,%cmbr,%jkpss,% iwona have sfbf and bf greek letters
  rmdefault=nxlmi,%ccr,%mdput,%iwona,%jkpvos,% nxlmi=newtxmath-libertine
  ]{isomath}% use \<vector,matrix,tensor>sym{A}
  % provides math fonts and defines math{sfbf}

 % If auto was chosen then figure it out -----------------------------
 \ifthenelse{\boolean{compilauto}}%
 {
  \RequirePackage{ifluatex}
  \ifluatex
   \setboolean{compillua}{true}
  \else
   \setboolean{compillua}{false}
  \fi
 }{}
 
 \ifthenelse{\boolean{compillua}}% ----------------------------------
 {%
  % CASE: LUALATEX --------------------------------------------------
  \RequirePackage[no-math]{fontspec}% no-math because it can collide with math fonts (accents, etc.)
  % since no-math was passed to fontspec we need to be more specific
  % -----------------------------------------------------------------
  % The following commented lines seem to mess up for \sf\upshape %\RequirePackage[oldstyle]{libertine}
  %\setmainfont{EB Garamond}[ Numbers=OldStyle,Ligatures=TeX, BoldFont=EBGaramond-Medium,]%Regular,%SemiBold, %BoldFeatures={Color=111111}
  \setmainfont{Linux Libertine O}[Ligatures=TeX,Numbers=OldStyle]
  \setsansfont{Linux Biolinum O}[Ligatures=TeX,Numbers=OldStyle]
  \setmonofont{Inconsolatazi4-Regular.otf}[Scale=MatchLowercase]
  % Below: deal with math fonts manually ----------------------------
  %\DeclareSymbolFont{operators}{OT1}{ntxtlf}{m}{n} 
  %\SetSymbolFont{operators}{bold}{OT1}{ntxtlf}{b}{n}
  %\DeclareSymbolFont{operators}{\encodingdefault}{\familydefault}{\mddefault}{n}
  %\DeclareMathAlphabet{\mathit}{\encodingdefault}{\familydefault}{\mddefault}{it}
  %\DeclareMathAlphabet{\mathbf}{\encodingdefault}{\familydefault}{\bfdefault}{n}
  \DeclareSymbolFont{stmry}{U}{stmry}{m}{n}
 }{%
  % CASE: PDFLATEX --------------------------------------------------
  \RequirePackage[oldstyle]{libertine}
  \RequirePackage[varqu,varl,]{inconsolata}%varqu,varl,narrow
 }%
}%
{}

% Common font declarations
\RequirePackage{fontawesome}

% --------------------------------------------------------------------
% Tufte-latex fonts adjustments --------------------------------------
\RequirePackage{microtype}% provides \textls[amount]{text}
\RequirePackage{letterspace}%
\RequirePackage{textcase} % provides \MakeTextUppercase and \MakeTextLowercase
% Set up letterspacing (using microtype package) -- requires pdfTeX v1.40+
% That implementation omes from the classic-thesis class
\newcommand{\allcapsTufte}[1]{\textls[100]{\uppercase{#1}}}%
\newcommand{\smallcapsTufte}[1]{\scshape\textls[50]{\MakeTextLowercase{#1}}}%
\newcommand{\allcaps}[1]{\MakeTextUppercase{#1}}%
\newcommand{\smallcaps}[1]{\scshape\MakeTextLowercase{#1}}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLOATS =============================================================

% For tables
\RequirePackage{array}
\RequirePackage{booktabs}
\RequirePackage{tabularx}

% For figures
\RequirePackage{graphicx}

% For captions
\RequirePackage[skip=2pt]{caption} % example skip set to 2pt
\RequirePackage{subcaption}
%\setlength\floatsep{2pt}
%\setlength\intextsep{2pt}\setlength{\intextsep}{-1ex}
\setlength\abovecaptionskip{2pt}
\setlength\belowcaptionskip{2pt}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PAGE STYLE - CHAPTERS AND SECTIONS ================================

\RequirePackage{titlesec}% TODO: read doc
\RequirePackage[pages=some]{background}

% Provide a chaptertoc command
\RequirePackage{titletoc}
%\setcounter{tocdepth}{1}% section only in main TOC
% Use the line above to redefine depth in document, if necessary (e.g. before and after \tableofcontents
%\setcounter{secnumdepth}{5}% no numbering further

\newcommand{\chaptertoc}{
 \startcontents[chaptoc]
 \printcontents[chaptoc]{}{1}{\section*{\contentsname}}
}

% Provide a chapter-page environment with fullwidth contents
% See fullwidth definition further
\newenvironment{chapterpage}[1][No title specified]{
 %\loadgeometry{default}
 \chapter{#1}
 %\vfill
 \startcontents[mychaptertoc]
 \contentsmargin{1em}
 \printcontents[mychaptertoc]{chaClassic}{1}{}
  %\vfill
 \vspace{50pt}% 50pt is what is left above the title. Gives some symmetry?
 \section*{Résumé}
}{
 %\end{fullwidth}
 %\clearpage
 %\loadgeometry{myTufte}
}

\titlecontents{chaClassicsection}[   2em]{\vspace{1ex}\scshape}{\contentslabel{2em}}{}{\titlerule*[1ex]{ }\contentspage}[]%, \thecontentspage
\titlecontents{chaClassicsubsection}[5em]{         }{\contentslabel{3em}}{}{\titlerule*[1ex]{.}\contentspage}[]%, \thecontentspage

% Part ---------------------------------------------------------------

\titleformat{name=\part}[display]%
 {\relax}% format
 {
  %\begin{tikzpicture}[remember picture, overlay]
  % \fill[boxcolor]($(current page.north west)+(\hoffset-1in+\marginparsep+\evensidemargin,-\voffset-\headsep+\topmargin-\headheight)$) 
  % rectangle ++($(\marginparwidth+\textwidth,-1.5\textheight)$);
  %\end{tikzpicture}%
  \raggedleft\Huge
  \smallcapsTufte{\partname~\thepart}%
 }% label
 {0pt}% horizontal (vertical in display) sep between title label and body
 {\color{emphColor}\raggedleft\Huge\bfseries\allcapsTufte}% before title body [ \normalsize\vspace*{.8\baselineskip}]

% Chapter ------------------------------------------------------------

\titleformat{name=\chapter,numberless}[display]%
 {\relax}% format
 {
  % Below: the fat gray rectangle
  %\begin{tikzpicture}[remember picture, overlay]
  % \fill[boxcolor]($(current page.north west)+(\hoffset-1in+\marginparsep+\evensidemargin+\textwidth,-\voffset-\headsep+\topmargin-\headheight)$) 
  % rectangle ++($(\marginparwidth,-1.5\textheight)$);
  %\end{tikzpicture}
  \begin{tikzpicture}[remember picture, overlay]
   \node[below left,inner sep=0pt, outer sep=0pt,] at 
   ($(current page.north east)+(-20.6mm,-\voffset-1in-\topmargin-\headheight-\headsep-50pt)$) 
    {\bfseries\centering\color{emphColor}\fontsize{120}{40}\selectfont *} ;
  \end{tikzpicture}
 }% label
 {0pt}% horizontal (vertical in display) sep between title label and body
 {\raggedright\Huge\smallcapsTufte}% before title body
 [\normalsize\vspace*{1.\baselineskip}]% after title body\titlerule

\titleformat{\chapter}[display]%
 {\relax}% format
 {% 
  \begin{tikzpicture}[remember picture, overlay]
   \node[left,inner sep=0pt, outer sep=0pt,] at 
   ($(current page.north east)+(-20.6mm,-\voffset-1in-\topmargin-\headheight-\headsep-50pt)$) 
    { \bfseries\color{emphColor} \fontsize{120}{40}\selectfont \thechapter } ;
  \end{tikzpicture}
  %\leavevmode\marginpar{\bfseries\centering\color{emphColor}\fontsize{120}{40}\selectfont\thechapter \\}
 }% label
 {0pt}% horizontal (vertical in display) sep between title label and body
 {\raggedright\Huge\smallcapsTufte}% before title body
 [\normalsize\vspace*{1.\baselineskip}]% after title body\titlerule

\titlespacing*{\chapter}{0pt}{1\baselineskip}{1.2\baselineskip}

% The rest -----------------------------------------------------------
\titleformat{\section}
 {\relax}%
 {\textsc{\bfseries\color{emphColor}\thesection}}%
 {1em}%
 {\large\bfseries\smallcapsTufte}%
\titlespacing*{\section}{0pt}{1.25\baselineskip}{1\baselineskip}

\titleformat{\subsection}%
 {\relax}%
 {\textsc{\bfseries\color{emphColor}\thesubsection}}%
 {1em}%
 {\normalsize\bfseries}%
\titlespacing*{\subsection}{0pt}{1.25\baselineskip}{1\baselineskip}

\titleformat{\subsubsection}%
 {\relax}%
 {\textsc{\color{emphColor}\thesubsubsection}}%
 {1em}%
 {\bfseries\normalsize\itshape}%
\titlespacing*{\paragraph}{0pt}{1\baselineskip}{1\baselineskip}

\titleformat{\paragraph}[runin]%
 {\normalfont\normalsize}%
 {}%
 {0pt}%
 {\scshape}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PAGE STYLE - HEADERS AND (NO) FOOTERS =============================

\RequirePackage{fancyhdr}

% Clear all
\fancyhead{}
\fancyfoot{}

% Let the header range over margins
\newlength{\myoddoffset}
\setlength{\myoddoffset}{\marginparwidth}
\addtolength{\myoddoffset}{\marginparsep}
\ifthenelse{\boolean{oneSidedPlease}}{% 
 \fancyheadoffset[rh]{\myoddoffset}% 
 \fancyhead[L]{\nouppercase{\itshape\footnotesize\leftmark}}
 %\fancyhead[L]{\nouppercase{\itshape\footnotesize\rightmark}}
 \fancyhead[R]{\thepage}
}{
 \fancyheadoffset[leh,roh]{\myoddoffset}
 \fancyhead[ER]{\nouppercase{\itshape\footnotesize\leftmark}}
 \fancyhead[OL]{\nouppercase{\itshape\footnotesize\rightmark}}
 \fancyhead[EL,OR]{\thepage}
}

% Contents of head/foot
\renewcommand{\headrulewidth}{0pt}

% Activate
%\fancypagestyle{empty}{\fancyhf{}} % clear header and footer fields 
\fancypagestyle{plain}{\fancyhf{}} % clear header and footer fields 
\pagestyle{fancy}

% Contents when cleardoublepage is used
\makeatletter
	\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
		\hbox{}
		\vspace*{\fill}
		\begin{center}
			%Find something cool to draw% content
		\end{center}
		\vspace{\fill}
		\thispagestyle{empty}
		\newpage
		\if@twocolumn\hbox{}\newpage\fi\fi\fi}  %\fi = fin fct \if
	\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% USING THE MARGIN ==================================================

% By default only the \marginpar command is provided
\setlength\marginparpush{\baselineskip}
% see https://tex.stackexchange.com/questions/60477/remove-space-after-figure-and-before-text/285193
%\setlength{\textfloatsep}{\baselineskip plus 0.2\baselineskip minus 0.2\baselineskip}
\setlength{\textfloatsep}{1ex plus .7ex minus.1ex}% space between top float and the following text 

\RequirePackage{sidenotes}
% This package provide everything we need:
% - environments marginfigure and margintable
% - environments figure* and table* (full width)
% - command sidecaption[<lof/lot entry>][<offset>]{<caption>}
%   to replace caption when needed
% - command sidenote for numbered marginnotes

% We leave marginpar alone to use it intact in other contexts, possibly
% TODO: redirect footnotes to sidenotes

% Now anything in the margin should be footnotesized
\RequirePackage{marginnote}% used by sidenotes anyways
\WarningFilter{latex}{Marginpar on page}
\renewcommand*{\marginfont}{\normalfont\footnotesize}
% For sidenotes we re-use the package implementation and add a size instruction
\makeatletter
\RenewDocumentCommand\sidenotetext{oo+m}{%
 \IfNoValueOrEmptyTF{#1}{%
  \@sidenotes@placemarginal{#2}{\textsuperscript{\thesidenote}{}~\footnotesize#3}%
  \refstepcounter{sidenote}%
 }{%
  \@sidenotes@placemarginal{#2}{\textsuperscript{#1}~#3}%
 }%
}
\makeatother

% Provide a command to drop citations in the margin
\newcommand\sidecite[2][]{%
 \sidenote[][#1]{\footnotesize%
  {\tiny\faPencilSquareO}%
  ~\citet{#2}.%
 }%
}%

% FULLWIDTH environment ====================================

% The following code should be used *after* any changes to the margins and
% page layout are made (e.g., after the geometry package has been loaded).
\newlength{\fullwidthlen}
\setlength{\fullwidthlen}{\marginparwidth}
\addtolength{\fullwidthlen}{\marginparsep}
\newenvironment{fullwidth}{%
 \begin{adjustwidth*}{}{-\fullwidthlen}%
}{%
 \end{adjustwidth*}%
}

\newcommand{\marginstomp}[1]{%
 \begin{fullwidth}
  #1
 \end{fullwidth}
}

%\RequirePackage{tocloft}
%%\renewcommand{\cftdot}{\textendash}
%%\renewcommand{\cftXpresnum}{SOMETHING }
%%\renewcommand{\cftXdotsep}{\cftnodots}
%%\setlength{\cftbeforesecskip}{-1cm}
%\setlength{\cftsecindent}{0ex}
