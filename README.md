# latex-two-thirds

*A(nother) Tufte-like class.*

`book`-based class with margins half as wide as text; initially meant to typeset a [PhD dissertation](https://hal.archives-ouvertes.fr/tel-02437346).

- The satisfying thing are: 
  - floats can be put in the margin, right next to where they are referred to;
  - tight floats do not need to break the text continuity;
  - the text is not too big and lines not too long;
  - the reader's eyes find a comfortable vertical frame to hang on.
- Lot is inspired from classes [`tufte-latex`](https://ctan.org/pkg/tufte-latex) and [`classic-thesis`](https://ctan.org/pkg/classicthesis). 
  - The implementation is here simpler hence more readable and easily modifiable. 
  - Although for most applications, *the aforementioned classes are clearly better*.
- Basic features were provided by the excellent [stackexchange answer](https://tex.stackexchange.com/questions/275565/tufte-layout-in-painless-memoir/275626#275626) from user [*Reign of Error*](https://tex.stackexchange.com/users/78941/reign-of-error).

The template was reused by 
[Vasudevan Kamasamudram](https://theses.hal.science/tel-03663421), 
[Raphaël Langlois](https://theses.hal.science/tel-04397571v2), 
[Tauno Tiirats](https://theses.fr/2021ECDN0004).
