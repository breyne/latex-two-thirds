#!/bin/sh

######################################################################
RED='\033[0;33m'
NC='\033[0m' # No Color

######################################################################

dir=$(pwd)

echo -e ${RED}
echo [2*install/3] LINK FILES TO LOCAL TEXMF
echo [2*install/3] Initialize usertree
echo -e ${NC}
tlmgr init-usertree

#cd ~/texmf
cd $(kpsewhich -var-value TEXMFHOME) 
mkdir -p tex/latex/local/ && cd tex/latex/local/

######################################################################
echo -e ${RED}
echo [2*install/3] source directory: $dir
echo [2*install/3] destination dir : $(pwd)
echo -e ${NC}

ln -s ${dir}/two-thirds.cls .

echo -e ${RED}
echo [2*install/3] rebuild index
echo -e ${NC}
texhash $(kpsewhich -var-value TEXMFHOME)

echo -e ${RED}
echo [2*install/3] Over. 
echo -e ${NC}
